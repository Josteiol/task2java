package log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private static String logPath = "./log/log.txt";
    String time;
    long startTime;
    long endTime;
    long elapsedTime;

    // Starts the timer and save current time and date
    public void startTimer(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        this.time = formatter.format(date);
        this.startTime = System.nanoTime();
    }

    // Stops the timer and calculates elapsed time
    public void stopTimer(){
        this.endTime = System.nanoTime();
        this.elapsedTime = this.endTime - this.startTime;
    }

    public String getTime(){
        return this.time;
    }

    public long getElapsedTime(){
        return this.elapsedTime;
    }

    public void log(String message){
        try {

            // Open given file in append mode.
            BufferedWriter out = new BufferedWriter(
                    new FileWriter(logPath, true));
            out.write(time+" "+message+" Function call took "+this.elapsedTime+"MS to execute."+"\n");
            out.close();
        }
        catch (IOException e) {
            System.out.println("exception occoured" + e);
        }
    }
}
