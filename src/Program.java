import java.io.IOException;
import java.util.Scanner;
import fileMan.FileManipulator;

public class Program {

    public static void main(String[] args) throws IOException {
        boolean active = true;
        Scanner scanner = new Scanner(System.in);
        while(active){

            System.out.println("What do you want todo?");
            System.out.println("Type the number of the action you want to do.");
            System.out.println("1: Print the path to all files.");
            System.out.println("2: Print path to all files with a specific file extension.");
            System.out.println("3: More options for manipulating the text file.");
            System.out.println("Or type exit to quit the program.");

            String action = scanner.nextLine();


            // List all files
            if(action.equals("1")){

                FileManipulator.PrintAllFiles();
            }

            // If user wants to filter by extension
            else if(action.equals("2")){
                System.out.println("What is the extension?");
                String extension = scanner.nextLine();
                FileManipulator.PrintAllWithEx(extension);


            }
            else if(action.equals("3")){
                System.out.println("What do you want to do with the textfile?");
                System.out.println("1: Print the nameof the file.");
                System.out.println("2: Print the textfiles size in bytes");
                System.out.println("3: Print the number of lines in the file.");
                System.out.println("4: Check in a word exists in  the file.");
                System.out.println("5: Check how many times a  word occurs in the file.");
                System.out.println("Or type exit to quit the program.");

                String action2 = scanner.nextLine();

                // Prints the name of the file.
                if(action2.equals("1")){
                    FileManipulator.fileName();

                }
                // Prints the filesize of the file.
                else if(action2.equals("2")) {
                    FileManipulator.fileSize();
                }
                //Prints the number of lines in the file.
                else if(action2.equals("3")) {
                    FileManipulator.fileLines();

                }
                // Checks if a word exists in the file.
                else if(action2.equals("4")) {
                    System.out.println("What is the word?");
                    String action3 = scanner.nextLine();
                    FileManipulator.wordInFile(action3);

                }
                // Counts the numberof times a word occurs in the file.
                else if(action2.equals("5")) {
                    System.out.println("What is the word?");
                    String action3 = scanner.nextLine();
                    FileManipulator.howManyWord(action3);

                }
                // Exits the program
                else if(action2.equals("exit")){
                    System.out.println("Bye");
                    active = false;
                }
                else{
                    System.out.println("Illegal input!");
                }

            }
            // Exits the program
            else if(action.equals("exit")){
                System.out.println("Bye");
                active = false;
            }

            // If the user typed in illegal argument
            else{
                System.out.println("Illegal input!");
            }
        }
    }
}
