package fileMan;

import log.Logger;
import java.io.*;
import java.util.Scanner;

public class FileManipulator {

    private static String directoryPath = "./Files";



    public static void PrintAllFiles(){
        Logger logger = new Logger();
        logger.startTimer();

        String[] pathnames;
        File f = new File(directoryPath);
        pathnames = f.list();

        for (String pathname : pathnames) {
            System.out.println(pathname);
        }

        logger.stopTimer();
        logger.log("Printed all files.");
    }

    public static void PrintAllWithEx(String extension){
        Logger logger = new Logger();
        logger.startTimer();

        File file = new File(directoryPath);
        File[] files = file.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if(name.toLowerCase().endsWith(extension)){
                    return true;
                } else {
                    return false;
                }
            }
        });
        for(File f:files){
            System.out.println(f.getName());
        }

        logger.stopTimer();
        logger.log("Printed all files with the "+extension+" extension.");
    }
    //Prints the name of the file
    public static void fileName(){
        Logger logger = new Logger();
        logger.startTimer();

        File f = new File(directoryPath+"/Dracula.txt");

        String fileName = f.getName();
        System.out.println("The name of the file is "+fileName);
        logger.stopTimer();
        logger.log("Printed the filename.");
    }

    // Prints the size of the file in bytes
    public static void fileSize(){
        Logger logger = new Logger();
        logger.startTimer();

        File f = new File(directoryPath+"/Dracula.txt");

        long fileSize = f.length();
        System.out.println("The size of the file in bytes "+fileSize);

        logger.stopTimer();
        logger.log("Printed the filesize.");
    }

    // Prints the number of lines  in the file
    public static void fileLines() throws IOException {
        Logger logger = new Logger();
        logger.startTimer();

        File f = new File(directoryPath+"/");

        BufferedReader reader = new BufferedReader(new FileReader(directoryPath+"/Dracula.txt"));
        int lines = 0;
        while (reader.readLine() != null) lines++;
        reader.close();
        System.out.println("The number of lines in the file is "+lines);

        logger.stopTimer();
        logger.log("Printed the number of lines in the file.");
    }

    // Checks if the word exits in the file
    public static void wordInFile(String word) throws FileNotFoundException {
        Logger logger = new Logger();
        logger.startTimer();

        File f = new File(directoryPath+"/Dracula.txt");

        boolean inFile =  (new Scanner(f).useDelimiter("\\Z").next()).contains(word);
        if(inFile) {
            System.out.println("The word is in the file");
        }
        else{
            System.out.println("The word is NOT in the file");
        }

        logger.stopTimer();
        logger.log("Looked for the word "+word+" in the file.");
    }

    //Checks how many times a word occurs in the text
    public static void howManyWord(String word){
        Logger logger = new Logger();
        logger.startTimer();

        try {
            File f = new File(directoryPath+"/Dracula.txt");
            Scanner myReader = new Scanner(f);
            int count = 0;
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                String[] words = line.split(" ");

                for(String wordFromFile: words){
                    if(wordFromFile.toLowerCase().equals(word.toLowerCase())){
                        count++;
                    }
                }
            }
            myReader.close();
            System.out.println(count);
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        logger.stopTimer();
        logger.log("Counted the number of times the word "+word+" appeared in  the file.");
    }
}
